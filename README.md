## Comment utiliser ce programme ?

Pour utiliser ce TIPE il vous faut :
 * python 3.6>
 * numpy
 * matplotlib
 * pygame

Ensuite pour utiliser le réseau, vous devez lancer l'application :
`python3 main.py` avec un des arguments suivant :
 * `-uid` pour essayer avec l'interface graphique
 * `-eval` pour faire une évaluation rapide du réseau
 * `-uit` pour montrer des chiffres de MNIST au réseau, cette commande vous ouvre une page matplotlib, le chiffre prédit est au dessus de l'image
 * `-term` pour lancer le processus d'entrainement sur un réseau `[28**2, 16, 16, 10]`

Si vous voulez enregistrer votre réseau, changer le prefix dans `net.save_weights`. N'oubliez pas d'ajouter le prefix dans la fonction `load_weights` !

Il est possible que vous deviez installer la police : Latin Modern Roman pour faire fonctionner l'interface graphique. Cependant elle est disponible sur internet : [lien vers la police](https://www.fontsquirrel.com/fonts/latin-modern-roman).

Si vous avez un problème n'hésitez pas à contacter : **planchonpa@eisti.eu**
from neural.mnist_utils.import_utils import *
from neural.ffnn.network import *

import multiprocessing
import time
import csv

import signal

def train_target():
    with open("res", "w") as file:
        shapes = [
            [28**2, 392, 196, 98, 49, 24 , 10], # decroissance du réseau (entonoir)
            [28**2, 392, 196, 98, 49, 10],
            [28**2, 392, 196, 98, 10],
            [28**2, 392, 196, 10],
            [28**2, 392, 10],
            [28**2,  16, 16 , 10], # taille des poids
            [28**2,  32, 32 , 10],
            [28**2,  64, 64 , 10],
            [28**2, 128, 128, 10],
            [28**2, 256, 256, 10],
            [28**2, 32, 32, 32, 32, 32 ,10], # taille de la couche
            [28**2, 32, 32, 32, 32 ,10],
            [28**2, 32, 32, 32 ,10],
            [28**2, 32, 32 ,10],
            [28**2, 32 ,10],
            [28**2, 16, 10],
            [28**2, 10]
        ]
        for shape in shapes:
            net = FeedForwardNetwork(shape, 10, 10, 3.0, file)
            net.train()

    # i = 0
    # with open("res", "w") as file:
    #     while True:
    #         time.sleep(.1)
    #         file.write(str(i))
    #         i += 1

class TimeoutException(Exception):
    pass

def timeout_handler(signum, frame):
    raise TimeoutException

signal.signal(signal.SIGALRM, timeout_handler)

signal.alarm(5*60)    
try:
    train_target()
    pass
except TimeoutException:
    print('function terminated')
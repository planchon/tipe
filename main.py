import sys

import contextlib

with contextlib.redirect_stdout(None):
	import pygame.font, pygame.event, pygame.draw

from neural.mnist_utils.ui import *
from neural.mnist_utils.import_utils import *
from neural.ffnn.network import *

# Partie pour entrainer le réseau
# On peut changer la taille du réseau, les epochs et les iterations
# aussi que le coef d'apprentissage
def terminal():
	net = FeedForwardNetwork([28**2, 16, 16, 10])
	training_data, validation_data, test_data = load_data()
	net.import_data(training_data, test_data)
	net.train(20, 10, 2.0)
	# n'hesitez pas a changer le prefix pour enregistrer vos propres réseau !
	net.save_weights("neural/data/", prefix="1616")
	# net.load_weights("neural/data")

# Pour faire un test, en utilisant les images de MNIST
def gui_test():
	net = FeedForwardNetwork([784, 392, 196, 98, 49, 10])
	net.load_weights("neural/data")	

	training_data, validation_data, test_data = load_data()
	net.import_data(training_data, test_data)

	net.show_multiple_prediction((3,3))

# Pour faire un test simple du réseau
def evaluate():
	net = FeedForwardNetwork([784, 392, 196, 98, 49, 10])
	training_data, validation_data, test_data = load_data()
	net.import_data(training_data, test_data)
	net.load_weights("neural/data")
	print("Prédictions correctes : " + str(net.evaluate(net.test_data)) + "/" + str(len(net.test_data)))

# Pour utiliser l'interface graphique
def gui_draw():
	print("dans la partie graphique, on utilise un reseau qui est deja entraine c:")

	net = FeedForwardNetwork([784, 392, 196, 98, 49, 10])
	net.load_weights("neural/data")	

	training_data, validation_data, test_data = load_data()
	net.import_data(training_data, test_data)

	size = [565,330]
	last_pos = (0, 0)
	pygame.init()
	screen = pygame.display.set_mode(size)
	pygame.display.set_caption("Interface graphique TIPE")
	running = True

	button = 0

	canvas = pygame.Surface((280,280))
	canvas.fill((255, 255, 255))

	draw_interface(screen)
	button_list = [[(0,280,280,330), "Envoyer", (90, 285)],[(285,280,565,330), "Nettoyer", (380, 285)]]
	draw_button(screen, button_list[0][1], button_list[0][2], button_list[0][0])
	draw_button(screen, button_list[1][1], button_list[1][2], button_list[1][0])

	while running:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				running = False

			if (pygame.mouse.get_pressed() == (1, 0, 0)):
				draw_canvas(canvas, event.pos, last_pos, (0,0,0))
				button = render_button(screen, event, button_list)

				if button == 0:
					image = transform_surface_to_pixelate(canvas)
					draw_pixel_image(screen, image)
					image = np.array(image)
					image = image.reshape(784, 1)
					result, label = net.test_image(image)
					h = ['0','1','2','3','4','5','6','7','8','9']
					y_pos = np.arange(len(h))
					liste = [x[0] for x in label]
					plt.bar(y_pos, liste)
					plt.title(result)

					plt.show()

				if button == 1:
					canvas.fill((255, 255, 255))

				screen.blit(canvas, (0,0))

			if (pygame.mouse.get_pressed() == (0, 0, 1)):
				draw_canvas(canvas, event.pos, last_pos, (255,255,255))
				screen.blit(canvas, (0,0))

			if event.type == pygame.MOUSEMOTION:
				render_button(screen, event, button_list)
				last_pos = event.pos

			pygame.display.flip()

if __name__ == '__main__':
	if (len(sys.argv) == 2)   and (sys.argv[1] == "-uid"):
		gui_draw()
	elif (len(sys.argv) == 2) and (sys.argv[1] == "-uit"):
		gui_test()
	elif (len(sys.argv) == 2) and (sys.argv[1] == "-eval"):
		evaluate()
	elif (len(sys.argv) == 2) and (sys.argv[1] == "-term"):
		terminal()
	else:
		print("les options sont : ")
		print(" -uid  : lancer l'interface graphique")
		print(" -eval : pour evaluer un 1 test")
		print(" -uit  : lancer la rennaissance sur les chiffres mnist")
		print(" -term : pour entrainer")
import random
import numpy as np
import matplotlib.pyplot as plt
import sys

np.random.seed(28091999)

class FeedForwardNetwork(object):
    def __init__(self, shape, epoch, batch_size, gamma, file, init = "random", folder = None):
        self.num_layers = len(shape)
        self.epoch = epoch
        self.batch_size = batch_size
        self.gamma = gamma
        self.shape = shape
        self.file = file
        self.training_data = []
        self.test_data = []

        if (init == "random"):
            self.randomize_wait(shape)
        elif (init == "load"):
            self.load_weights(folder)

    def randomize_wait(self, shape):
        self.biases = [np.random.randn(y, 1) for y in shape[1:]]
        self.weights = [np.random.randn(y, x)
                        for x, y in zip(shape[:-1], shape[1:])]

    def save_weights(self, folder, prefix=""):
        np.save(folder + "/" + prefix + "w.npy", self.weights)
        np.save(folder + "/" + prefix + "b.npy", self.biases)

    def load_weights(self, folder, prefix=""):
        self.weights = np.load(folder + "/" + prefix + "w.npy", allow_pickle=True)
        self.biases  = np.load(folder + "/" + prefix + "b.npy", allow_pickle=True)

    def feedforward(self, a):
        for b, w in zip(self.biases, self.weights):
            a = self.sigmoid(np.dot(w, a)+b)
        return a

    def import_data(self, training_data, test_data):
        self.training_data = list(training_data)
        self.test_data     = list(test_data)

    def sigmoid(self, z, prime=False):
        if (not prime):
            return 1.0/(1.0+np.exp(-z))
        else:
            return self.sigmoid(z)*(1 - self.sigmoid(z))

    def choose_label(self, label_arr):
        return np.argmax(label_arr)

    def test_image(self, image):
        image = np.array(image).reshape(784, 1)
        label = self.feedforward(image)
        return self.choose_label(label), label

    def show_multiple_prediction(self, shape):
        fig = plt.figure(figsize=shape)
        for i in range(1, shape[0] * shape[1]+ 1):
            image_index = random.randint(0, len(self.test_data) - 1) 
            img = self.test_data[image_index][0]
            label = self.feedforward(img)
            fig.add_subplot(shape[0], shape[1], i)
            fig.subplots_adjust(wspace = 1, hspace = 1)
            plt.title(str(self.choose_label(label)))
            plt.imshow(img.reshape(28,28), cmap='Greys')

        plt.show()

    def single_image_test(self):
        image_index = random.randint(0, len(self.test_data))
        image = self.test_data[image_index][0]
        label = self.feedforward(image)
        plt.imshow(image.reshape(28, 28), cmap='Greys')
        plt.title(str(self.choose_label(label)))
        plt.axis("off")
        plt.show()

    def train(self):
        n = len(self.training_data)
        n_test = len(self.test_data)

        last_test = 0.0
        for j in range(self.epoch):

            random.shuffle(self.training_data)
            mini_batches = [self.training_data[k:k+ self.batch_size] for k in range(0, n, self.batch_size)]

            for mini_batch in mini_batches:
                self.train_batch(mini_batch, self.gamma)
            
            last_test = (self.evaluate(self.test_data) / n_test) * 100
            self.write_res(last_test)
            print("Epoch {} finis, COST : {:.2f}".format(j, last_test))

        print("\n\nEntrainement fini !\n")

    def write_res(self, cost):
        self.file.write(str(self.shape) + ', ' + str(self.epoch) + ', ' + str(self.batch_size) + ', ' + str(self.gamma) + ', ' + str(cost))

    def full_feed_forward(self, x):
        activation = x
        activations = [x] 
        zs = []
        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation)+b
            zs.append(z)
            activation = self.sigmoid(z)
            activations.append(activation)

        return activations, zs

    def train_batch(self, mini_batch, eta):
        nabla_biais  = [np.zeros(b.shape) for b in self.biases]
        nabla_weight = [np.zeros(w.shape) for w in self.weights]

        # on créé le nabla_biais et nabla_weight
        for x, y in mini_batch:
            delta_nabla_biais, delta_nabla_weight = self.backprop(x, y)
            nabla_biais  = [last_biais  + update_biais  for last_biais , update_biais  in zip(nabla_biais , delta_nabla_biais )]
            nabla_weight = [last_weight + update_weight for last_weight, update_weight in zip(nabla_weight, delta_nabla_weight)]

        # on met a jour les poids et biais
        self.weights = [w - (eta/len(mini_batch)) * nw for w, nw in zip(self.weights, nabla_weight)]
        self.biases = [b - (eta/len(mini_batch)) * nb for b, nb in zip(self.biases, nabla_biais)]    
    
    def backprop(self, x, y):
        nabla_biais  = [np.zeros(b.shape) for b in self.biases]
        nabla_weight = [np.zeros(w.shape) for w in self.weights]
        
        activations, zs = self.full_feed_forward(x)

        delta = (activations[-1] - y) * self.sigmoid(zs[-1], prime=True)
        nabla_biais[-1] = delta
        nabla_weight[-1] = np.dot(delta, activations[-2].transpose())

        # -l on part de la fin de l'array
        for l in range(2, self.num_layers):
            z = zs[-l]
            delta = np.dot(self.weights[-l+1].transpose(), delta) * self.sigmoid(z, prime=True)
            nabla_biais[-l] = delta
            nabla_weight[-l] = np.dot(delta, activations[-l-1].transpose())

        return (nabla_biais, nabla_weight)

    def evaluate(self, test_data):
        test_results = [(np.argmax(self.feedforward(x)), y)
                        for (x, y) in test_data]
        return sum([int(x == y) for (x, y) in test_results])
import numpy as np

# pour que pygame ne dise rien 
import contextlib
with contextlib.redirect_stdout(None):
	import pygame.font, pygame.event, pygame.draw

no_active = (150, 150, 150)
hover_c   = (200, 200, 200)

# pour faire le rendu du text d'un bouton
def draw_button(screen, text, text_pos, rect, hover=False):
	if not hover:
		pygame.draw.rect(screen, no_active, rect)		
	if hover:
		pygame.draw.rect(screen, hover_c, rect)		

	text_render = font_big.render(text, 1, (0,0,0))
	screen.blit(text_render, text_pos)

# pour le faire le rendu d'un bouton et les interactions
def render_button(screen, event, button_list):
	click = -1
	for button in button_list:
		if (event.pos[0] < button[0][2]) and (event.pos[0] > button[0][0]) and (event.pos[1] > button[0][1]) and (event.pos[1] < button[0][3]):
			draw_button(screen, button[1], button[2], button[0], hover=True)
			click = button_list.index(button)
		else:
			draw_button(screen, button[1], button[2], button[0])

	return click

def draw_canvas(screen, now, last, color):
	draw_line(screen, now, last, color)

# pixelise l'image
def transform_surface_to_pixelate(canvas):
	canvas = pygame.transform.smoothscale(canvas, (28, 28))
	image = pygame.surfarray.array3d(canvas)
	image = abs(1-image/253)
	image = np.mean(image, 2)

	image = image.transpose()
	image = image.ravel()

	return image

# affiche l'image pixelisée
def draw_pixel_image(screen, image):
	image = (255 - image * 255)
	scale = 10
	for i in range(len(image)):
		pygame.draw.rect(screen, (image[i], image[i], image[i]), (285 + i % 28 * 10, i // 28 * 10, 10, 10))

def draw_line(screen, now, last, color):
	dx = last[0]-now[0]	
	dy = last[1]-now[1]
	distance = max(abs(dx), abs(dy))

	for i in range(distance):
		x = int(now[0]+i/distance*dx)
		y = int(now[1]+i/distance*dy)
		pygame.draw.circle(screen, color, (x, y), 12)

def draw_interface(screen):
	global font_big

	font_big = pygame.font.SysFont("Latin Modern Roman", 24)

	pygame.draw.rect(screen, (255, 255, 255), (0, 0, 280, 280))
	pygame.draw.rect(screen, (255, 255, 255), (285, 0, 565, 280))

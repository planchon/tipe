import numpy as np
import csv
from matplotlib import pyplot as plt

import pickle
import gzip

# ouvre la data brute et la reshape pour la futur utilisation
# dans le réseau
# FORMAT : 
# 50000 tuples (array de 784 lignes (nombre), array de 10 lignes (result)) (training)
# 10000 tuples (array de 784 lignes (nombre), int (result)) (test)
def load_data():
	mnist_file = gzip.open('neural/data/mnist.pkl.gz', 'rb')
	training_raw, validation_raw, test_raw = pickle.load(mnist_file, encoding="latin1")
	training_data = zip([np.reshape(x, (784, 1)) for x in training_raw[0]], [vectorized_result(y) for y in training_raw[1]])
	validation_data = zip([np.reshape(x, (784, 1)) for x in validation_raw[0]], validation_raw[1])
	test_data = zip([np.reshape(x, (784, 1)) for x in test_raw[0]], test_raw[1])
	mnist_file.close()
	return (training_data, validation_data, test_data)

# retourne un nombre en vecteur ex: 2 => [0,0,1,0, ..., 0]
def vectorized_result(j):
	e = np.zeros((10, 1))
	e[j] = 1.0
	return e
